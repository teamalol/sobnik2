module.exports = {
  mode: 'universal',
  webfontloader: {
    events: false,
    google: {
      families: ['Montserrat:400,500,600:cyrillic&display=swap']
    },
    timeout: 5000
  },
  plugins: ['@plugins/vuetify'],
  telemetry: false,
  vue: {
    config: {
      productionTip: process.env.NODE_ENV === 'production' ? false : true,
      devtools: process.env.NODE_ENV === 'production' ? false : true,
      silent: process.env.NODE_ENV === 'production' ? true : false
    }
  },

  modules: [
    '@nuxtjs/axios',
    [
      '@nuxtjs/yandex-metrika',
      {
        id: '56551738',
        webvisor: true
        // clickmap:true,
        // useCDN:false,
        // trackLinks:true,
        // accurateTrackBounce:true,
      }
    ],
    ['@nuxtjs/google-gtag', { id: 'UA-150448215-2' }]
  ],
  axios: {
    // https: true,
    credentials: true,
    // proxy: true,
    baseUrl: process.env.NODE_ENV === 'production' ? 'https://sobnik.ru' : 'http://localhost',
    browserBaseURL: process.env.NODE_ENV === 'production' ? 'https://sobnik.ru' : 'http://localhost'
  },



  buildModules: [
    '@nuxtjs/moment'
    // With options
    // [
    //   '@nuxtjs/vuetify',
    //   {
    //     defaultAssets: {
    //       font: {
    //         family: 'Roboto'
    //       },
    //       icons: 'mdi'
    //     }
    //   }
    // ]
  ],

  head: {
    // title: 'sobnik',
    htmlAttrs: {
      lang: 'ru'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1, maximum-scale=1 shrink-to-fit=no' }
      // { hid: 'description', name: 'description', content: 'Sobnik.ru - аренда без посредников' }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },
  /*
   ** Customize the progress bar color
   */
  loading: { color: '#3B8070' },
  /*
   ** Build configuration
   */
  build: {
    cache: true,
    /*
     ** Run ESLint on save
     */
    extend(config, { isDev, isClient }) {
      if (isDev) {
        config.devtool = isClient ? 'source-map' : 'inline-source-map'
      }

      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
