const state = () => ({
  ads: [],
  adsFiltered: [],
  pagesCount: 1,
  adsCountOnPage: 10,
  adsByPages: [],
  pageCurrent: 1,
  wordSearched: '',
  accountAds: [],
  adminAds: [],
  currentPost: {}
})

const mutations = {
  setAds(state, ads) {
    state.ads = ads
  },
  getAds(state, ads) {
    let pagesCount = ads.length / state.adsCountOnPage

    state.adsFiltered = ads
    state.pagesCount = pagesCount < 1 ? 1 : Math.ceil(pagesCount)
    let adsByPages = []
    adsByPages.push([])

    let pageNumber = 0,
      adNumber = 0

    for (let adIdx = 0; adIdx < state.adsFiltered.length; adIdx++) {
      adsByPages[pageNumber].push(state.adsFiltered[adIdx])
      if (adsByPages[pageNumber].length === state.adsCountOnPage) {
        ++pageNumber
        adNumber = 0
        adsByPages.push([])
      }
      ++adNumber
    }

    state.adsByPages = adsByPages
    state.pageCurrent = 1
  },

  selectPage(state, value) {
    state.pageCurrent = value
  },
  addPost(state, ads) {
    state.accountAds = [...ads]
  },

  setAccountAds(state, ads) {
    state.accountAds = ads
  },
  setAdminAds(state, ads) {
    state.adminAds = ads
  },
  setCurrentPost(state, post) {
    state.currentPost = post
  },
  deletePost(state, id) {
    state.ads = state.ads.filter(item => item.id !== id)
    state.accountAds = state.accountAds.filter(item => item.id !== id)
  }
}

const actions = {
  getAccountAds({ commit }) {
    return new Promise((resolve, reject) => {
      this.$axios
        .$get('/api/ads/account')
        .then(response => {
          commit('setAccountAds', response)
          resolve()
        })
        .catch(err => {
          console.log(err)
          reject()
        })
    })
  },
  getAdminAds({ commit }) {
    return new Promise((resolve, reject) => {
      this.$axios
        .$get('/api/ads/admin')
        .then(response => {
          commit('setAdminAds', response)
          resolve()
        })
        .catch(() => reject())
    })
  },
  deletePost({ commit }, post) {
    this.$axios
      .$post('/api/ads/delete', post)
      .then(() => {
        commit('deletePost', post.id)
      })
      .catch(console.error)
  },
  deleteAllAdsOfUser({ commit }, userId) {
    this.$axios
      .$post('/api/ads/deleteAllAdsOfUser', { userId: userId })
      .then(() => {})
      .catch(console.error)
  },
  complain({ commit }, post) {
    this.$axios
      .$post('/api/ads/complain', post)
      .then(() => {})
      .catch(console.error)
  },
  getAds({ commit, dispatch }, { citySelected, adType }) {
    this.$axios
      .$post('/api/ads/list/500', {
        adType: adType,
        citySelected: citySelected
      })
      .then(response => {
        commit('setAds', response)
        commit('getAds', response)
        //commit("authorization/setIsAuthenticated");
        dispatch('authorization/setIsAuthenticated', null, { root: true })
      })
      .catch(err => {
        if (err.response.status === 401) dispatch('authorization/deleteTokens', null, { root: true })
      })
  },

  getAdsLimit({ commit, dispatch }, { citySelected, adType }) {
    this.$axios
      .$post('/api/ads/limit/', {
        adType: adType,
        citySelected: citySelected
      })
      .then(response => {
        commit('setAds', response)
        commit('getAds', response)
        //commit("authorization/setIsAuthenticated");
      })
      .catch(console.error)
  },

  getPostById({ commit }, postId) {
    return new Promise((resolve, reject) => {
      this.$axios
        .$get(`/api/ads/getPostById/${postId}`)
        .then(response => {
          commit('setCurrentPost', response[0])
          resolve()
        })
        .catch(err => {
          console.log(err)
          if (err.response.status === 401) commit('authorization/deleteTokens')
          reject()
        })
    })
  },
  getPost({ commit }, postId) {
    return new Promise((resolve, reject) => {
      this.$axios
        .$get(`/api/ads/${postId}`)
        .then(response => {
          commit('setCurrentPost', response[0])
          resolve()
        })
        .catch(err => {
          console.log(err)
          if (err.response.status === 401) commit('authorization/deleteTokens')
          reject()
        })
    })
  },
  addPost({ commit }, formData) {
    this.$axios
      .$post('/api/add_post/', formData)
      .then(response => {
        commit('setAccountAds', response)
      })
      .catch(err => {
        if (!!err.response) {
          if (err.response.status === 401) commit('authorization/deleteTokens')
          //  router.push('/')
        }
      })
  },
  editPost({ commit }, formData) {
    this.$axios
      .$post('/api/edit_post/', formData)
      .then(response => {
        commit('setAccountAds', response)
      })
      .catch(err => {
        if (!!err.response) {
          if (err.response.status === 401) commit('authorization/deleteTokens')
          //  router.push('/')
        }
      })
  },
  filterAdsByText({ commit, state }, wordSearched) {
    let ads = state.ads
    if (!!wordSearched && wordSearched.length > 0) {
      let word = wordSearched
      ads = ads.filter(ad => {
        return ad.text.search(word) !== -1 ? true : false
      })
    }
    commit('getAds', ads)
  },
  selectPage({ commit }, value) {
    commit('selectPage', value)
  }
}

const getters = {
  adsByPages(state) {
    return state.adsByPages[state.pageCurrent - 1]
  }
}

export default {
  state,
  actions,
  mutations,
  getters
}
