import Cookies from 'js-cookie'

const state = () => ({
  citys: [
    { id: 1, label: 'Москва', name: 'msk', soc: 'msk' },
    { id: 64, label: 'Кемерово', name: 'kemerovo', soc: 'kemerovo' },
    { id: 97, label: 'Новокузнецк', name: 'novokuznetsk', soc: 'kuz' },
    { id: 9115, label: 'Анжеро-Судженск', name: 'anzhero-sudzhensk' },
    { id: 994, label: 'Белово', name: 'belovo' },
    { id: 1129124, label: 'Берёзовский', name: 'berezovskiy' },
    { id: 1178, label: 'Гурьевск', name: 'gurevsk' },
    { id: 18746, label: 'Калтан', name: 'kaltan' },
    { id: 5915, label: 'Киселёвск', name: 'kiselevsk' },
    { id: 1809, label: 'Ленинск-Кузнецкий', name: 'leninsk-kuznetskiy' },
    { id: 5955, label: 'Мариинск', name: 'mariinsk' },
    { id: 1129456, label: 'Междуреченск', name: 'mezhdurechensk' },
    { id: 468, label: 'Прокопьевск', name: 'prokopevsk', soc: 'prok' },
    { id: 258, label: 'Юрга', name: 'yurga' },
    { id: 2, label: 'Санкт-Петербург', name: 'spb', soc: 'spb' },
    { id: 49, label: 'Екатеринбург', name: 'ekaterinburg', soc: 'ekb' },
    { id: 60, label: 'Казань', name: 'kazan', soc: 'kazan' },
    { id: 99, label: 'Новосибирск', name: 'novosibirsk', soc: 'novosibirsk' },
    { id: 95, label: 'Нижний Новгород', name: 'nizhniy-novgorod', soc: 'nizhniy_novgorod' },
    { id: 158, label: 'Челябинск', name: 'chelyabinsk', soc: 'chelyabinsk' },
    { id: 104, label: 'Омск', name: 'omsk', soc: 'omsk' },
    { id: 123, label: 'Самара', name: 'samara', soc: 'samara' },
    { id: 119, label: 'Ростов-на-Дону', name: 'rostov-na-donu' },
    { id: 151, label: 'Уфа', name: 'ufa', soc: 'ufa' },
    { id: 73, label: 'Красноярск', name: 'krasnoyarsk', soc: 'krasnoyarsk' },
    { id: 42, label: 'Воронеж', name: 'voronezh', soc: 'voronezh' },
    { id: 110, label: 'Пермь', name: 'perm', soc: 'perm' },
    { id: 10, label: 'Волгоград', name: 'volgograd', soc: 'volgograd' },
    { id: 72, label: 'Краснодар', name: 'krasnodar' },
    { id: 125, label: 'Саратов', name: 'saratov' },
    { id: 147, label: 'Тюмень', name: 'tumen' },
    { id: 143, label: 'Тольятти', name: 'tolyatti' },
    { id: 56, label: 'Ижевск', name: 'izhevsk' },
    { id: 25, label: 'Барнаул', name: 'barnaul' },
    { id: 149, label: 'Ульяновск', name: 'ulyanovsk' },
    { id: 57, label: 'Иркутск', name: 'irkutsk' },
    { id: 153, label: 'Хабаровск', name: 'habarovsk', soc: 'habarovsk' },
    { id: 169, label: 'Ярославль', name: 'yaroslavl' },
    { id: 37, label: 'Владивосток', name: 'vladivostok', soc: 'vladivostok' },
    { id: 85, label: 'Махачкала', name: 'mahachkala' },
    { id: 144, label: 'Томск', name: 'tomsk', soc: 'tomsk' },
    { id: 106, label: 'Оренбург', name: 'orenburg' },
    { id: 122, label: 'Рязань', name: 'ryazan' },
    { id: 122, label: 'Астрахань', name: 'astrahan' },
    { id: 88, label: 'Набережные челны', name: 'chelny' },
    { id: 109, label: 'Пенза', name: 'penza' },
    { id: 66, label: 'Киров', name: 'kirov' },
    { id: 78, label: 'Липецк', name: 'lipezk' },
    { id: 157, label: 'Чебоксары', name: 'cheboksary' },
    { id: 61, label: 'Калининград', name: 'kaliningrad' },
    { id: 146, label: 'Тула', name: 'tula' },
    { id: 24, label: 'Балашиха', name: 'balashiha' },
    { id: 75, label: 'Курск', name: 'kursk' },
    { id: 185, label: 'Севастополь', name: 'sevastopol' },
    { id: 133, label: 'Сочи', name: 'sochi' },
    { id: 134, label: 'Ставрополь', name: 'stavropol' },
    { id: 148, label: 'Улан-Удэ', name: 'ulan-ude' },
    { id: 141, label: 'Тверь', name: 'tver' },
    { id: 82, label: 'Магнитогорск', name: 'magnitogorsk' },
    { id: 33, label: 'Брянск', name: 'bryansk' },
    { id: 55, label: 'Иваново', name: 'ivanovo' },
    { id: 26, label: 'Белгород', name: 'belgorod' },
    { id: 136, label: 'Сургут', name: 'surgut' },
    { id: 39, label: 'Владимир', name: 'vladimir' },
    { id: 96, label: 'Нижний Тагил', name: 'tagil' },
    { id: 161, label: 'Чита', name: 'chita' },
    { id: 22, label: 'Архангельск', name: 'arhangelsk' },
    { id: 627, label: 'Симферополь', name: 'simpheropol' },
    { id: 62, label: 'Калуга', name: 'kaluga' },
    { id: 130, label: 'Смоленск', name: 'smolensk' },
    { id: 40, label: 'Волжский', name: 'volzhskiy' },
    { id: 124, label: 'Саранск', name: 'saransk' },
    { id: 168, label: 'Якутск', name: 'yakutsk' },
    { id: 8, label: 'Череповец', name: 'cherepovec' },
    { id: 74, label: 'Курган', name: 'kurgan' },
    { id: 105, label: 'Орёл', name: 'orel' },
    { id: 41, label: 'Вологда', name: 'vologda' },
    { id: 38, label: 'Владикавказ', name: 'vladikavkaz' },
    { id: 270, label: 'Подольск', name: 'podolsk' },
    { id: 160, label: 'Грозный', name: 'grozniy' },
    { id: 87, label: 'Мурманск', name: 'murmansk' },
    { id: 140, label: 'Тамбов', name: 'tambov' },
    { id: 112, label: 'Петрозаводск', name: 'petrozavodsk' },
    { id: 135, label: 'Стерлитамак', name: 'sterlitamak' },
    { id: 93, label: 'Нижневартовск', name: 'nizhnevartovsk' },
    { id: 71, label: 'Кострома', name: 'kostroma' },
    { id: 98, label: 'Новороссийск', name: 'novorossisk' },
    { id: 59, label: 'Иошкар-Ола', name: 'ioshkar-ola' },
    { id: 155, label: 'Химки', name: 'himki' },
    { id: 139, label: 'Таганрог', name: 'taganrog' },
    { id: 70, label: 'Комсомольск-на-Амуре', name: 'komsomolsk-na-amure' },
    { id: 138, label: 'Сыктывкар', name: 'siktivkar' },
    { id: 94, label: 'Нижнекамск', name: 'nizhnekamsk' },
    { id: 90, label: 'Нальчик', name: 'нальчик' },
    { id: 164, label: 'Шахты', name: 'shahti' },
    { id: 621, label: 'Дзержинск', name: 'dzerzhinsk' },
    { id: 108, label: 'Орск', name: 'orsk' },
    { id: 32, label: 'Братск', name: 'bratsk' },
    { id: 30, label: 'Благовещенск', name: 'blagoveshensk' },
    { id: 1469, label: 'Энгельс', name: 'engels' },
    { id: 19, label: 'Ангарск', name: 'angarsk' },
    { id: 859, label: 'Королёв', name: 'korolev' },
    { id: 35, label: 'Великий Новгород', name: 'velikiy-novgorod' },
    { id: 274, label: 'Старый Оскол', name: 'oskol' },
    { id: 312, label: 'Мытищи', name: 'mitishi' },
    { id: 114, label: 'Псков', name: 'pskov' },
    { id: 80, label: 'Люберцы', name: 'luberzi' },
    { id: 167, label: 'Южно-Сахалинск', name: 'yuzhno-sahalinsk' },
    { id: 27, label: 'Бийск', name: 'biysk' },
    { id: 21, label: 'Армавир', name: 'armavir' },
    { id: 9930, label: 'Балаково', name: 'balakovo' },
    { id: 121, label: 'Рыбинск', name: 'ribinsk' },
    { id: 17, label: 'Абакан', name: 'abakan' },
    { id: 11, label: 'Северодвинск', name: 'severodvinsk' },
    { id: 102, label: 'Норильск', name: 'norilsk' },
    { id: 113, label: 'Петропавловск-Камчатский', name: 'petropalovsk' },
    { id: 150, label: 'Уссурийск', name: 'ussuriysk' },
    { id: 3700, label: 'Волгодонск', name: 'volgodonsk' },
    { id: 952, label: 'Сызрань', name: 'sizran' },
    { id: 101, label: 'Новочеркасск', name: 'novocherkask' },
    { id: 1573, label: 'Каменск-уральский', name: 'kamensk-uralskiy' },
    { id: 794, label: 'Златоуст', name: 'zlatoust' },
    { id: 483, label: 'Красногорск', name: 'krasnogorsk' },
    { id: 580, label: 'Электросталь', name: 'elektrostal' },
    { id: 18, label: 'Альметьевск', name: 'almetevsk' },
    { id: 264, label: 'Салават', name: 'salavat' },
    { id: 7159, label: 'Миасс', name: 'miass' },
    { id: 478, label: 'Керчь', name: 'kerch' },
    { id: 91, label: 'Находка', name: 'nahodka' },
    { id: 5098, label: 'Копейск', name: 'kopeisk' },
    { id: 116, label: 'Пятигорск', name: 'pyatygorsk' },
    { id: 1185, label: 'Хасавюрт', name: 'hasavurt' },
    { id: 69, label: 'Коломна', name: 'kolomna' },
    { id: 424, label: 'Рубцовск', name: 'rubcovsk' },
    { id: 899, label: 'Березники', name: 'berezniki' },
    { id: 83, label: 'Майкоп', name: 'maykop' },
    { id: 4890, label: 'Одинцово', name: 'odinzovo' },
    { id: 68, label: 'Ковров', name: 'kowrov' },
    { id: 67, label: 'Кисловодск', name: 'kislovodsk' },
    { id: 1054308, label: 'Домодедово', name: 'domodedovo' },
    { id: 243, label: 'Нефтекамск', name: 'neftekamsk' },
    { id: 382, label: 'Нефтеюганск', name: 'nefteyugansk' },
    { id: 4712, label: 'Батайск', name: 'bataysk' },
    { id: 2451, label: 'Новочебоксарск', name: 'novocheboksarsk' },
    { id: 1297, label: 'Щёлково', name: 'shelkovo' },
    { id: 340, label: 'Серпухов', name: 'serpuhov' },
    { id: 171, label: 'Дербент', name: 'derbent' },
    { id: 964, label: 'Новомосковск', name: 'novomoskovsk' },
    { id: 2001, label: 'Черкесск', name: 'cherkesk' },
    { id: 89, label: 'Назрань', name: 'nazran' },
    { id: 3102, label: 'Каспийск', name: 'kaspiysk' },
    { id: 107, label: 'Орехово-Зуево', name: 'orehovo-zuevo' },
    { id: 76, label: 'Кызыл', name: 'kizil' },
    { id: 103, label: 'Обнинск', name: 'obninsk' },
    { id: 20950, label: 'Новый Уренгой', name: 'noviy-urengoi' },
    { id: 92, label: 'Невинномысск', name: 'nevinnomisk' },
    { id: 117, label: 'Раменское', name: 'ramenskoe' },
    { id: 476, label: 'Димитровград', name: 'dimitrovgrad' },
    { id: 9196, label: 'Октябрьский', name: 'oktyabrskiy' },
    { id: 857, label: 'Долгопрудный', name: 'dolgoprudniy' },
    { id: 542, label: 'Камышин', name: 'kamishin' },
    { id: 52, label: 'Ессентуки', name: 'essentuki' },
    { id: 807, label: 'Муром', name: 'murom' },
    { id: 53, label: 'Жуковский', name: 'zhukowskiy' },
    { id: 799, label: 'Евпатория', name: 'evpatoriya' },
    { id: 461, label: 'Новошахтинск', name: 'novoshahtinsk' },
    { id: 126, label: 'Северск', name: 'seversk' },
    { id: 459, label: 'Реутов', name: 'reutov' },
    { id: 1991, label: 'Артём', name: 'artem' },
    { id: 403, label: 'Ноябрьск', name: 'noyabrsk' },
    { id: 882, label: 'Ачинск', name: 'achinsk' },
    { id: 1057953, label: 'Пушкино', name: 'pushkino' },
    { id: 20, label: 'Арзамас', name: 'arzamas' },
    { id: 1133, label: 'Бердск', name: 'berdsk' },
    { id: 1386, label: 'Сергиев Посад', name: 'sergiev-posad' },
    { id: 51, label: 'Елец', name: 'elec' },
    { id: 166, label: 'Элиста', name: 'elista' },
    { id: 722, label: 'Ногинск', name: 'noginsk' },
    { id: 1489, label: 'Новокуйбышевск', name: 'novokuybishevsk' },
    { id: 4644, label: 'Железногорск', name: 'zheleznogorsk' }
  ],
  // citySelected: localStorage.getItem('cityId') || 1,

  citySelected: Cookies.get('cityId') || 1,
  // citySelectedLabel: '',
  // citySelectedName: '',
  adType: 1,
  filter: {
    cityId: 1,
    adType: 0
  }

  // adType: localStorage.getItem('adType') || 1
})

const mutations = {
  setCityId(state, id) {
    state.citySelected = Number(id)
    // state.citySelectedLabel = state.citys.filter(city => city.id === Number(id))[0].label
    // state.citySelectedName = state.citys.filter(city => city.id === Number(id))[0].name
  },
  setAdType(state, id) {
    state.adType = id
  }
}

const actions = {
  setCityId({ commit }, { citySelected, adType }) {
    return new Promise((resolve, reject) => {
      commit('setCityId', citySelected)
      commit('setAdType', adType)

      localStorage.setItem('cityId', citySelected)
      Cookies.set('cityId', citySelected)
      localStorage.setItem('adType', adType)
      resolve()
    })
  },
  setCityIdByName({ state, commit }, cityName) {
    let citySelected = state.citys.filter(city => city.name === cityName)[0].id
    commit('setCityId', citySelected)
    return new Promise((resolve, reject) => {
      localStorage.setItem('cityId', citySelected)
      Cookies.set('cityId', citySelected)

      resolve(citySelected)
    })
  }
}
const getters = {
  getCityNameById: state => id => {
    return state.citys.filter(city => city.id === id)[0].label
  },

  citySelectedName: state => state.citys.filter(city => city.id === Number(state.citySelected))[0].name || '',

  citySelectedLabel: state => state.citys.filter(city => city.id === Number(state.citySelected))[0].label || '',

  getCitys: state => {
    function sortCitys(a, b) {
      const cityA = a.label.toUpperCase(),
        cityB = b.label.toUpperCase()
      let comparison = 0
      if (cityA > cityB) {
        comparison = 1
      } else if (cityA < cityB) {
        comparison = -1
      }
      return comparison
    }
    return state.citys.sort(sortCitys)
  }
}

export default {
  state,
  actions,
  mutations,
  getters
}
