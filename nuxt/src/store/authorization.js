// Authorization module
import Cookies from 'js-cookie'

const state = () => ({
  token: '',
  refreshToken: '',
  accessToken: '',
  isAuthenticated: false
})

const mutations = {
  loginUser(state, token) {
    state.token = token
  },
  setIsAuthenticated(state) {
    state.isAuthenticated = true
  },
  setTokens(state, tokens) {
    state.accessToken = tokens.accessToken
    state.refreshToken = tokens.refreshToken
    state.isAuthenticated = true
    // localStorage.setItem('at', tokens.accessToken)
    // localStorage.setItem('rt', tokens.refreshToken)

    // this.$axios.setToken(tokens.accessToken, "Bearer");
    // this.$axios.setToken(tokens.refreshToken, "refreshToken");
  },
  deleteTokens(state) {
    state.accessToken = ''
    state.refreshToken = ''
    state.isAuthenticated = false
    localStorage.removeItem('accessToken')
    localStorage.removeItem('refreshToken')
    Cookies.remove('accessToken')
    Cookies.remove('refreshToken')
  }
}

const actions = {
  loginUser({ commit }) {
    this.$axios
      .$get('/api/auth/')
      .then(response => {
        if (response.queryString) {
          window.location = response.queryString // редирект на авторизацию ВК
        }
        commit('loginUser', response.token)
      })
      .catch(err => {
        commit('deleteTokens')
      })
  },
  setTokens({ commit }, tokens) {
    commit('setTokens', tokens)
  },
  setIsAuthenticated({ commit }) {
    commit('setIsAuthenticated')
  },
  deleteTokens({ commit }) {
    commit('deleteTokens')
  }
}

export default {
  state,
  actions,
  mutations
}
