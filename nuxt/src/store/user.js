const state = () => ({
  photoAvatar: '',
  status: false
})

const mutations = {
  setUserData(state, user) {
    state.photoAvatar = user.photo
    state.firstName = user.firstName
  },
  setStatus(state) {
    state.status = true
  }
}

const actions = {
  setUserData({ commit }, user) {
    commit('setUserData', user)
  },
  setStatus({ commit }) {
    commit('setStatus')
  },
  blockUser({ commit, dispatch }, userId) {
    return new Promise((resolve, reject) => {
      this.$axios
        .post('/api/user/block', { userId: userId, reasonId: 100 })
        .then(response => {
          resolve()
        })
        .catch(err => {
          if (!!err.response) {
            if (err.response.status === 401) dispatch('authorization/deleteTokens', null, { root: true })

            //  router.push('/')
          }
          reject()
        })
    })
  }
}

export default {
  state,
  actions,
  mutations
}
