const state = () => ({
  list: []
})

const mutations = {
  buySubscription(state, subscription) {}
}

const actions = {
  addPost({ commit, dispatch }, suscription) {
    this.$axios
      .post('/api/subscription', suscription)
      .then(response => {
        commit('buySubscription', response.data)
      })
      .catch(err => {
        if (!!err.response) {
          if (err.response.status === 401) dispatch('authorization/deleteTokens', null, { root: true })

          //  router.push('/')
        }
      })
  }
}
