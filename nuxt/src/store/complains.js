const state = () => ({
  list: []
})

const mutations = {
  getComplainList(state, complains) {
    state.list = complains
  }
}

const actions = {
  getComplainList({ commit, dispatch }) {
    return new Promise((resolve, reject) => {
      this.$axios
        .get('/api/complains')
        .then(res => {
          commit('getComplainList', res.data)
          resolve()
        })
        .catch(err => {
          if (!!err.response) {
            if (err.response.status === 401) dispatch('authorization/deleteTokens', null, { root: true })

            //  router.push('/')
          }
          reject()
        })
    })
  }
}

export default {
  state,
  actions,
  mutations
}
