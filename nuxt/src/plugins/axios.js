export default function ({ $axios, app, store }) {
  $axios.onRequest(config => {
    if (!!store.state.authorization.accessToken) {
      config.headers.common['Authorization'] = `Bearer ${store.state.authorization.accessToken}`
    }
  })
}
