const companyNames = ['ан ', 'недвижимост', 'агенство', 'агент', 'аренда','жил', 'квартир', 'дом'],
      positions = ['агент', 'риэлтор', 'недвижимост', 'жил', 'квартир', 'дом']  

module.exports = {
    companyNames: companyNames,
    positions: positions
}
