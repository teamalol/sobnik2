'use strict'
const EventEmitter = require('events')
const emitter = new EventEmitter()
emitter.setMaxListeners(100)
let filters = require('./filters')
let cron = require('node-cron')
let count = 0

let DB = require('./DB')
const easyVKhelper = require('./helpers/easyVKhelper')
const { connect, getVKClient} = require("./client");

let period = 21 * 60

function delay() {
  return new Promise((resolve) => setTimeout(resolve, 2000))
}


const startApp = async () => {
  let responsesVK = []
  await connect()
  console.log('Старт программы')
  let end_time = parseInt(Date.now() / 1000)
  let start_time = end_time - period
  console.log('st_time', Date.now())
  count++

  console.log('Запуск №', count)

  await Promise.resolve()
      .then(() => {
        return getVKClient().call('newsfeed.search', {
          q:'сдам квартир -сутки -секс -диагноз -потеряшка -посуточно -кредит -ипотек -закупк -товар -"фотограф" -фотосъемка -hozya -фотосессия -форекс -ru -airbnb -таиланд -"котёнка" -"передержка"', //
          extended: 1,
          fields: 'city, home_town, career, photo_50, status, name, description',
          start_time,
          end_time,
          count: 200
        })
        return res
      })
      .then(async (resSdamKvartiru) => {
        await delay()
        responsesVK.push(resSdamKvartiru)
        return getVKClient().call('newsfeed.search', {
          q:'сдам комнат -ru -сутки -секс -диагноз -потеряшка -посуточно -кредит -ипотек -закупк -товар -"фотограф" -фотосъемка -форекс -hozya -фотосессия -airbnb -таиланд -"котёнка" -"передержка"', //
          extended: 1,
          count: 200,
          fields: 'city, home_town, career, photo_50, status, name, description',
          start_time,
          end_time
        })
      })
      .then(async (resSdamKomnatu) => {
        await delay()
        responsesVK.push(resSdamKomnatu)
        return getVKClient().call('newsfeed.search', {
          q:'сниму квартир -ru -сутки -секс -диагноз -потеряшка -посуточно -кредит -ипотек -закупк -товар -"фотограф" -форекс -фотосъемка -hozya -фотосессия -airbnb -таиланд -"котёнка" -"передержка"', //
          extended: 1,
          count: 200,
          fields: 'city, home_town, career, photo_50, status, name, description',
          start_time,
          end_time
        })
      })
      .then(async (resSnimuKvartiru) => {
        await delay()
        responsesVK.push(resSnimuKvartiru)
        return getVKClient().call('newsfeed.search', {
          q:'сниму комнат -ru -сутки -секс -диагноз -потеряшка -посуточно -кредит -ипотек -закупк -товар -"фотограф" -форекс -фотосъемка -hozya -фотосессия -airbnb -таиланд -"котёнка" -"передержка"', //
          extended: 1,
          count: 200,
          fields: 'city, home_town, career, photo_50, status, name, description',
          start_time,
          end_time
        })
      })
      .then((resSnimuKomnatu) => {
        responsesVK.push(resSnimuKomnatu)
        return Promise.resolve(responsesVK)
      })
      .then(filters.defineAccountType)
      .then(async (res) => {
        let blackList = await DB.getUserBlacklist()
        return {res, blackList}
      })
      .then(filters.searchUserBlacklist)
      .then(filters.isSmallQuantityPosts)
      .then(filters.defineAgentCareer)
      .then(async (res) => {
        let workGroups = []
        if (res.workGroupIds.length > 0) {
          workGroups = [...await easyVKhelper.getGroupsByIds(res.workGroupIds)]
        }
        return {res, workGroups}
      })
      .then(filters.defineAgentCareerByWorkGroup)
      .then(filters.getCityIdFromProfiles)
      .then(filters.searchIdenticalAds)
      .then(filters.getPostByCity)
      .then(filters.formationQueryString)
      .catch(console.error)
  process.on('unhandledRejection', console.error)
}

const start = async ()=> {
  cron.schedule('*/20 * * * *', async ()=>{
    await startApp()
  })
  await startApp()
}

start()
