'use strict'
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
const EventEmitter = require('events')
const emitter = new EventEmitter()
emitter.setMaxListeners(100)
const config = require('../config')
const easyVK = require('easyvk')
let cron = require('node-cron')
let pool = require('mysql2/promise').createPool(config.server.mysql)

function startApp() {
  function delay(ms) {
    return new Promise((resolve, reject) => {
      setTimeout(resolve, ms)
    })
  }

  pool
    .getConnection()
    .then((conn) => {
      let query_str =
        'SELECT id, user_id, post_id, public_id FROM default_db.ads where ad_type=0 or ad_type=1 order by id desc LIMIT 1000'
      let posts = conn.query(query_str)

      conn.release()
      return posts
    })
    .then((posts) => {
      let adsArray = []
      posts[0].map((post) => {
        adsArray.push({
          id: post.id,
          wallId:
            (post.public_id > 0 ? (post.public_id * -1).toString() : post.user_id.toString()) +
            '_' +
            post.post_id.toString(),
        })
      })

      return adsArray
    })
    .then((posts) => {
      let arrayNumber = 0,
        postCountInArray = 100,
        postNumber = 0,
        postsByArrays = []
      postsByArrays.push([])

      for (let postIdx = 0; postIdx < posts.length; postIdx++) {
        postsByArrays[arrayNumber].push(posts[postIdx])
        if (postNumber >= postCountInArray - 1) {
          ++arrayNumber
          postNumber = 0
          postsByArrays.push([])
        }
        ++postNumber
      }
      return postsByArrays
    })
    .then((postsByArrays) => {
      for (let index = 0; index < postsByArrays.length; index++) {
        delay(20000 * index).then(() => {
          easyVK(config.server.easyvk)
            .then((vk) => {
              return vk.call('wall.getById', {
                posts: postsByArrays[index].map((post) => post.wallId),
              })
            })
            .then((response) => {
              let posts = response

              let confirmedPostsUserIdPostId = []
              for (const post of posts) {
                confirmedPostsUserIdPostId.push(post.owner_id.toString() + '_' + post.id)
              }
              let allPosts = postsByArrays[index]
              let postsDeleted = allPosts.filter((post) => !confirmedPostsUserIdPostId.includes(post.wallId))
              return postsDeleted
            })
            .then((postsDeleted) => {
              if (postsDeleted.length === 0) return Promise.resolve()
              let adsToBeDeletedStr = ''
              for (const post of postsDeleted) {
                adsToBeDeletedStr = adsToBeDeletedStr + ' OR id = ' + post.id.toString()
              }
              let queryPostDeleted =
                'DELETE FROM default_db.ads WHERE id = ' + postsDeleted[0].id.toString() + adsToBeDeletedStr + ';'

              pool.getConnection().then((conn) => {
                conn.query(queryPostDeleted)
                conn.release()
              })
              return Promise.resolve()
            })
            .catch(console.error)
        })
      }
    })
    .then(() => {
      pool.getConnection().then((conn) => {
        conn.query('CALL delete_dupes()')
        conn.release()
      })
    })
    .catch(console.error)
}

let task = cron.schedule('*/120 * * * *', startApp)

task.start()

startApp()
