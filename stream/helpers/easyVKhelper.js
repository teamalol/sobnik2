const {getVKClient} = require("../client");

async function getGroupsByIds(workGroupIds) {
  return Promise.resolve()
    .then(() => {
      return getVKClient().call('groups.getById', {
        group_ids: workGroupIds,
        fields: 'description,city',
      })
    })
    .then(resp => {
      return resp
    })
}

module.exports = {
  getGroupsByIds,
}
