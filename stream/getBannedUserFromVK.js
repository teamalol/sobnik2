process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
const config = require('./config')
const easyVK = require('easyvk')
var filters = require('./filters')
var DB = require('./DB')

let count = 0

function startApp() {
  console.log('Старт программы')
  let date_now = parseInt(Date.now() / 1000)
  let period = 60 * 5
  let st_time = date_now - period
  count++

  easyVK(config.server.easyvk).then(vk => {
    vk.call('groups.getBanned', {
      group_id: '68780309',
      count: 200,
      offset: 0,
    })

      .then(response =>
        response.items.map(el => {
          return { id: !!el.profile ? el.profile.id : 0, status: 110 }
        })
      )
      .then(users => {
        DB.addUserBlacklist(users)
      })
      .catch(console.error)
    console.log('Запуск №', count)
  })
  //Handle all rejects and errors
  process.on('unhandledRejection', console.error)
}

startApp()
