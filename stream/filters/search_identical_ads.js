function searchIdenticalAds(posts) {
  // массив уникальных постов
  let uni_arr = posts.filter(
    (thing, index, self) => self.findIndex(p => p.post.owner_id === thing.post.owner_id) === index
  )
  // посты от повтор.юзеров
  let bad_arr = posts.filter(
    (thing, index, self) => self.findIndex(p => p.post.owner_id === thing.post.owner_id) != index
  )

  console.log(`Постов: ${posts.length} Уникальных: ${uni_arr.length} Повторяющиеся: ${bad_arr.length}`)

  if (uni_arr.length) {
    console.log(`Уникальных постов: ${uni_arr.length}`)
    return Promise.resolve(uni_arr)
  } else {
    return Promise.reject(console.log('Нет подходящих постов'))
  }
}

module.exports.searchIdenticalAds = searchIdenticalAds
