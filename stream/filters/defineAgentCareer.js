let careers = require('../config/careers')
let statuBlackList = require('../config/statuBlackList')
let DB = require('../DB')

async function defineAgentCareer(res) {
  console.log('??? ОПРЕДЕЛЯЕМ МЕСТО РАБОТЫ')
  let publicIds = ''

  for (const post of res.white_posts) {
    if (post.publicId > 0) publicIds = publicIds + post.publicId + ','
  }

  let profilesNotAgents = []
  let agents = []
  let workGroupIds = ''
  for (let profile of res.profiles) {
    let isAgent = false
    if (!!profile.career) {
      if (profile.career.length > 0) {
        for (let career of profile.career) {
          for (let key of careers.companyNames) {
            if (!!career.company) {
              if (career.company.toLowerCase().search(key) !== -1) {
                isAgent = true
                console.log('агент найден по company')
                agents.push({ id: profile.id, status: 500 })
              }
            }
          }
          if (!isAgent) {
            for (let key of careers.positions) {
              if (!!career.position) {
                if (career.position.toLowerCase().search(key) !== -1) {
                  isAgent = true
                  console.log('агент найден по position')
                  agents.push({ id: profile.id, status: 500 })
                }
              }
            }
          }
        }
      }
      if (!isAgent) {
        if (profile.status.length > 2) {
          for (let key of statuBlackList) {
            if (profile.status.toLowerCase().search(key) !== -1 && !isAgent) {
              isAgent = true
              console.log('агент найден по status')
              agents.push({ id: profile.id, status: 400 })
            }
          }
        }
      }

      //получаем название группы по id
      if (!isAgent && !!profile.career) {
        if (profile.career.length > 0) {
          for (const career of profile.career) {
            if (!!career.group_id) workGroupIds = workGroupIds + career.group_id + ','
          }
        }
      }

      if (!isAgent) {
        console.log('признак агента не найден в карьере и в статусе')
        console.log('карьера отсутствует = ' + !profile.career.length)
        profilesNotAgents.push(profile)
      }
    }
  }

  if (agents.length > 0) {
    DB.addUserBlacklist(agents)
    //TODO: сделать codeBlackList | id | причина блокировки |
  }

  if (profilesNotAgents.length > 0) {
    let resp = {}
    resp.white_posts = res.white_posts
    resp.profiles = profilesNotAgents
    resp.workGroupIds = workGroupIds + publicIds
    return Promise.resolve(resp)
  } else {
    return Promise.reject(console.log('--- Поиск Агентов по КАРЬЕРЕ: все АГЕНТЫ !!!?'))
  }
}

module.exports = {
  defineAgentCareer: defineAgentCareer,
}
