let careers = require('../config/careers')

function defineAgentCareerByWorkGroup({ res, workGroups }) {
  if (!workGroups.length) {
    return Promise.resolve(res)
  }
  let blackListGoups = workGroups.filter(({name})=> {
    for (let key of careers.companyNames) {
      if (name.toLowerCase().search(key) !== -1) {
        return true
      }
    }
    return false
  })

  let blackListGoupIds = blackListGoups.map(group => group.id)

  let profiles = res.profiles.filter(profile => {
    let notAgent = true
    if (!!profile.career && profile.career.length > 0) {
      for (const career of profile.career) {
        if (blackListGoupIds.includes(career.group_id)) {
          notAgent = false
        }
      }
    }
    return notAgent
  })

  posts = []

  for (let index = 0; index < res.white_posts.length; index++) {
    const post = res.white_posts[index]
    for (const group of workGroups) {
      if (post.publicId === group.id) {
        res.white_posts[index].publicName = group.name
        res.white_posts[index].publicDescription = !!group.description ? group.description : ''
        res.white_posts[index].publicCity = !!group.city ? group.city : ''
      }
    }
  }

  if (profiles.length) {
    let resp = {}
    resp.white_posts = res.white_posts
    resp.profiles = profiles

    return Promise.resolve(resp)
  } else {
    return Promise.reject(console.log('--- Поиск Агентов по КАРЬЕРЕ: все АГЕНТЫ !!!?'))
  }
}

module.exports.defineAgentCareerByWorkGroup = defineAgentCareerByWorkGroup
