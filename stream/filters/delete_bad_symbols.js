// Удаляем ненужные символы
function deleteBadSymbols(posts) {
  console.log(' / Удаляем ненужные символы')
  for (let post of posts) {
    let regex = new RegExp('[^А-ЯЁа-яё0-9.,]', 'g');
    post.post.newtext = post.post.text.replace(regex, ' ');
  }

  return Promise.resolve(posts);
}


module.exports.deleteBadSymbols = deleteBadSymbols
