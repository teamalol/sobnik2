function isSmallQuantityPosts(res) {
    console.log('??? ФИЛЬТРАЦИЯ ПО КОЛИЧЕСТВУ ПОСТОВ')
    let posts = [];
    for (let post of res.white_posts) {
        if (post.id > 40) {
            posts.push(post)
        }
    }
    if (posts.length > 0) {
        let resp = {};
        resp.profiles = res.profiles;
        resp.white_posts = posts;
        return Promise.resolve(resp)
    } else {
        return Promise.reject(console.log('--- Нет постов с id > 40'))
    }

}

module.exports.isSmallQuantityPosts = isSmallQuantityPosts
