const config = require('./../config')
const words = config.userWallsBlackWords
let DB = require('./../DB')
const {getVKClient} = require("../client");

function formationQueryString(res) {
  let users = res.map((ad) => ad.post.owner_id)
  console.log('USERS:   ' + users)
  //массив строк запроса к АПИ ВК
  let arrayScriptStr = [],
    usersArrayByThree = []

  //  формируем массив масивов по 24 элемента в каждом(ограничение API)
  let threeArray = []
  for (let user of users) {
    threeArray.push(user)
    if (threeArray.length === 24 || threeArray.length === users.length) {
      usersArrayByThree.push(threeArray)
      threeArray = []
    }
  }

  // формируем массив запросов к АПИ
  for (let j = 0; j < usersArrayByThree.length; j++) {
    let scriptUsersWallGet = []
    for (let user of usersArrayByThree[j]) {
      let queryString = `"${user}": API.wall.get({"owner_id": ${user}, "count": 100, "filter": "owner"}).items@.text, `
      scriptUsersWallGet.push(queryString)
    }
    let scriptArr = ['return {', ...scriptUsersWallGet, '};']
    // формируем строку запроса к апи
    let scriptStr = scriptArr.join(' ')
    arrayScriptStr.push(scriptStr)
  }

  function generateWhiteListByUsersWalls(response) {
    let agents = []
    let whiteList = []
    Object.keys(response).forEach((id) => {
      let quantityPosts = response[id].length
      let quantityBadPosts = 0

      if(!parseInt(id)) {
        return
      }

      for (let post of response[id]) {
        let isContainWord = false
        for (let word of words) {
          if (post.toLowerCase().search(word) != -1) {
            isContainWord = true
            console.log('Найден пост содержащий минус слова' + id)
          }
        }
        if (isContainWord) ++quantityBadPosts
      }
      let procentBadPosts = parseInt((quantityBadPosts / quantityPosts) * 100)
      if (procentBadPosts > 20 && quantityPosts > 20) {
        console.log('Пользователь возможно агент, частота >20% ' + id)
        agents.push({ id: id, status: 300 })
      } else whiteList.push({ posts: response[id], id: id })
    })
    if (agents.length > 0) DB.addUserBlacklist(agents)
    return Promise.resolve(whiteList)
  }
  // Фильтруем изначально полученные посты по белому списку
  function filterPosts(whiteList) {
    let usersWhiteList = whiteList.map((user) => parseInt(user.id), 10)
    if (usersWhiteList.length > 0) {
      let postsWhiteList = res.filter((ad) => usersWhiteList.includes(ad.post.owner_id))
      if (postsWhiteList.length > 0) {
        console.log(' +++ Фильтр по стене Юзера: хороших постов = ' + postsWhiteList.length)
        return Promise.resolve(postsWhiteList)
      } else Promise.reject(console.log(' ---- Фильтр по стене Юзера: Нет постов '))
    } else Promise.reject(console.log(' ---- Фильтр по стене Юзера: Нет юзеров '))
  }

  function concatResponses(responses) {
    let wallsResponses = {}
    responses.forEach((response) => {
      Object.keys(response).forEach((wall) => (wallsResponses[wall] = response[wall]))
    })
    return Promise.resolve(wallsResponses)
  }

  allQuerys = []
  for (let i = 0; i < arrayScriptStr.length; i++) {
    let query = Promise.resolve()
      .then(() => {
        return getVKClient().call('execute', {
          code: arrayScriptStr[i],
          v: '5.92',
        })
      })
      .then((resp) => {
        return Promise.resolve(resp)
      })
      .catch((err) => console.log(err))
    allQuerys.push(query)
  }

  function deleteBadSymbols(posts) {
    console.log(' / Удаляем ненужные символы')
    for (let post of posts) {
      let regex = new RegExp('[^А-ЯЁа-яё0-9.,]', 'g')
      post.post.newtext = post.post.text.replace(regex, ' ')
    }
    let filteredPosts = posts.filter(
      (post) =>
        post.post.newtext.length > 5 &&
        post.post.newtext.length < 1300 &&
        post.post.post_type === 'post'
    ) //555398750 549218575
    let filteredByAttachments = filteredPosts.filter((post) => {
      if (post.post.attachments?.length) {
        for (const attach of post.post.attachments) {
          if (attach.type === 'link' || attach.type === 'article' || attach.type === 'note' || attach.type === 'page') {
            return false
          }
        }
        return true
      } else {
        return true
      }
    })
    let mainKeys = ['сда', 'сним', 'квартир', 'дом', 'жил', 'ищу', 'комнат', 'сня']
    let adsContainMainKey = filteredByAttachments.filter((post) => {
      let isContainKey = false
      for (const key of mainKeys) {
        if (post.post.text.toLowerCase().search(key) >= 0) {
          isContainKey = true
          return isContainKey
        }
      }
      return isContainKey
    })

    return Promise.resolve(adsContainMainKey)
  }

  Promise.all(allQuerys)
    .then((responses) => concatResponses(responses))
    .then(generateWhiteListByUsersWalls)
    .then(filterPosts)
    .then((posts) => deleteBadSymbols(posts))
    .then(DB.insertAdsIntoDb)
}

module.exports = {
  formationQueryString: formationQueryString,
}
