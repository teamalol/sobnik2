let city = require('../config/city')

let postsFiltered = []

let cityIds = city.cities.map(({id}) => id)

function getPostByCity(posts) {
    postsFiltered = posts.filter(({ gorod }) => cityIds.indexOf(gorod.id) > -1)

    if (postsFiltered.length) {
        console.log('В выбранных городах ' + postsFiltered.length + ' объявлений' )
        return Promise.resolve(postsFiltered)
    } else {
        return Promise.reject(console.log('Нет объявлений в выбранных городах'))
    }
}

module.exports.getPostByCity = getPostByCity
