function defineAccountType(res) {
  let posts = {
    is_users: [],
    profiles: [],
  }

  userIds = []
  signerIds = ''
  // Автор поста юзер или группа?

  function defineAccount(items, postType) {
    for (let post of items) {
      if (post.owner_id > 0 && !userIds.includes(post.owner_id)) {
        post.publicId = 0
        post.ad_type = postType
        posts.is_users.push(post)
        userIds.push(post.owner_id)
      }
    }
    for (let post of items) {
      if (post.owner_id < 0 && !!post.signer_id && !userIds.includes(post.signer_id)) {
        posts.is_users.push(post)
        post.ad_type = postType
        post.publicId = post.from_id * -1
        post.owner_id = post.signer_id
        userIds.push(post.signer_id)
        signerIds = String(post.signer_id) + ','
      }
    }
  }

  defineAccount(res[0].items, 1) //разбираем массив объявлений СДАМ квартиру
  defineAccount(res[1].items, 1) //разбираем массив объявлений СДАМ комнату
  defineAccount(res[2].items, 0) //разбираем массив объявлений СНИМУ квартиру
  defineAccount(res[3].items, 0) //разбираем массив объявлений СНИМУ комнату

  if (posts.is_users.length) {
    for (let response of res) {
      posts.profiles = posts.profiles.concat(response.profiles)
    }

    return Promise.resolve(posts)
  } else return Promise.reject(console.log('Нет подходящих постов'))
}

module.exports.defineAccountType = defineAccountType
