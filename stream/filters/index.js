const {defineAccountType} = require("./define_account_type")
const {deleteBadSymbols} = require("./delete_bad_symbols")
const {getCityIdFromProfiles} = require("./get_city_id_from_profile")
const {searchUserBlacklist} = require("./search_user_blacklist")
const {searchIdenticalAds} = require("./search_identical_ads")
const {getPostByCity} = require("./get_post_by_city")
const {defineAgentCareerByWorkGroup} = require("./defineAgentCareerByWorkGroup")
const { defineAgentCareer} = require("./defineAgentCareer")
const { isSmallQuantityPosts} = require("./isSmallQuantityPosts")
const { formationQueryString} = require("./scanUserWall")


module.exports = {
    defineAccountType ,
    deleteBadSymbols ,
    getCityIdFromProfiles ,
    searchUserBlacklist,
    searchIdenticalAds,
    getPostByCity,
    defineAgentCareer,
    isSmallQuantityPosts,
    formationQueryString,
    defineAgentCareerByWorkGroup
  }


