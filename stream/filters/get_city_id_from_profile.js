const city = require('../config/city'),
  blackListPublicName = require('../config/blackListPublicName').words

// Поиск города ВК


function getCityIdFromText(string) {
  for (let city_el of city.cities) {
    for (let key of city_el.title_arr) {
      if (string.toLowerCase().search(key) !== -1) {
        console.log(` + Город найден в тексте  = ${key}`)
        return city_el.id
      }
    }
  }
}

function getCityIdFromPublicName(post) {
  //Если пост размещен в группе , то ищем город в названии группы или ее описании. В противном случае игнорируем это объявление возвращая 0

  string = post.publicName
  for (let word of blackListPublicName) {
    if (string.toLowerCase().search(word) !== -1) {
      console.log(` - Объявление заблокировано по названию группы = ${word}`)
      return 0
    }
  }
  if (!!post.publicCity.id) {
    return post.publicCity.id
  } else {
    for (let city_el of city.cities) {
      for (let key of city_el.title_arr) {
        if (string.toLowerCase().search(key) !== -1) {
          console.log(` + Город найден в тексте  = ${key}`)
          return city_el.id
        }
      }
    }
  }
  return 0
}

function getCityIdFromProfiles(res) {
  let posts_with_gorod = []
  // Отбираем профили содержащих город
  function containCity(profile) {
    return profile.city != undefined || (profile.home_town != undefined && profile.home_town != '')
  }
  let profiles_contain_city = res.profiles.filter(containCity)

  // Склеиваем посты с городами по юзер ID
  for (let post of res.white_posts) {
    gorod = {}
    let contain_gorod = false

    console.log('Ищем город в тексте объявления')

    if (!gorod.id && !!post.publicName) {
      gorod.id = getCityIdFromPublicName(post)
    }
    if (!gorod.id ) {
      gorod.id = getCityIdFromText(post.text)
    }

    //  if (gorod.id == undefined && !!post.publicName) gorod.id = getCityIdFromText(post.publicName)
    if (!gorod.id) {
      posts_with_gorod.push({ post, gorod })
      contain_gorod = true
    } else console.log('город не найден в тексте')

    if (!contain_gorod) {
      for (let profile of profiles_contain_city) {
        if (post.owner_id == profile.id) {
          // присваиваем сити ID если он существ.
          if (profile.city) {
            gorod.id = profile.city.id
            posts_with_gorod.push({ post, gorod })
            contain_gorod = true
            console.log('Город найден в профиле')
          } else if (profile.home_town) {
            console.log('Ищем город по home_town')
            gorod.id = getCityIdFromText(profile.home_town)
            if (gorod.id != undefined) {
              posts_with_gorod.push({ post, gorod })
              contain_gorod = true
            } else console.log('город не найден в Тауне')
          }
        }
      }
    }
  }
  //console.log(posts_with_gorod)
  if (posts_with_gorod.length > 0) {
    console.log(' + + Поиск городов завершился успешно')
    // Присвоим аватарки в объект поста
    let avatars = {},
      names = {}
    res.profiles.map(profile => {
      avatars[profile.id] = profile.photo_50
      names[profile.id] = profile.first_name
    })

    posts_with_gorod.forEach(post => {
      post.post.avatar = avatars[post.post.owner_id]
      post.post.first_name = names[post.post.owner_id]
    })

    const profileIds = res.profiles.map(({id}) => id)
    let posts = posts_with_gorod.filter(({post}) => profileIds.includes(post.owner_id))
    return Promise.resolve(posts)
  } else {
    return Promise.reject(console.log(' - - Города не найдены'))
  }
}

module.exports.getCityIdFromProfiles = getCityIdFromProfiles
