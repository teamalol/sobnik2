function searchUserBlacklist({ res, blackList }) {
  let white_posts = []
  let whiteUserList = []
  for (let post of res.is_users) {
    let is_black = false
    for (let black_user of blackList) {
      if (post.owner_id === black_user) {
        is_black = true
        console.log(' - Юзер в черном списке ' + post.owner_id)
      }
    }

    if (!is_black) {
      console.log('+ Юзер отсутствует в черном списке ' + post.owner_id)
      white_posts.push(post)
      whiteUserList.push(post.owner_id)
    }
  }

  if (white_posts.length > 0) {
    console.log('Успешно пройден этап ЧС')
    let resp = {}
    resp.white_posts = white_posts
    resp.profiles = res.profiles.filter(profile => whiteUserList.includes(profile.id))
    return Promise.resolve(resp)
  } else {
    return Promise.reject(console.log('ЧС: Нет подходящих постов'))
  }
}

module.exports = {
  searchUserBlacklist: searchUserBlacklist,
}
