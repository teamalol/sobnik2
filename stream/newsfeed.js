process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
const config = require('./config');
const easyVK = require('easyvk');
var filters  = require ('./filters')


easyVK(config.server.easyvk)
  .then(vk => {
    const StreamingAPI = vk.streamingAPI;
    return StreamingAPI.connect({
      clientId: config.server.myApplicationId,
      clientSecret: config.server.myApplicationSecret,
    });
  })
  .then(({ connection, vk }) => {
    vk.call('users.get', {
      user_ids: authors_id_str,
      fields: 'city, country, home_town',
    }).then(res=>(console.log(res)))


    // connection.on('post',(post)=>(
    //   filters.defineAccountType(post)
    //   .then(filters.searchUserBlacklist)
    //   .then(filters.deleteBadSymbols)
    //   .then(filters.getCityIdFromText)
    //   .catch(error => console.error(error))
    // )
    // );

    //All errors listeners
    connection.on('error', console.error);
    connection.on('failure', console.error);
    connection.on('serviceMessage', console.log);

    const initRules = connection.initRules(
     config.vk_rules.rules,
      ({ where, rule, error }) => {
        console.error(`Occured error on ${where} method in ${rule} rule (${error})`);
      }
    );

    return new Promise(async (resolve, rejects) => {
      //send connection to next chain
      const changes = await initRules;
      changes.connection = connection;
      resolve(changes);
    });
  })
  .then(({ log: changes, vk, connection }) => {
    const interval = 10000;
    const countOfSends = 2;
    const start = new Date();

    console.log(changes);

    async function closeConnection() {
      return await connection.close();
    }

    async function deleteRules() {
      return await connection.deleteAllRules();
    }

    //Every `interval` milliseconds
    const intervalId = setInterval(() => {
      const now = new Date();
      console.log('Sended success!');
      if (now - start >= interval * countOfSends) {
        console.log('Closing connection....');

        closeConnection();
        filters.getCityIdFromVk(vk);
        clearInterval(intervalId);
        deleteRules();
      }
    }, interval);
  })
  .catch(console.error);

//Handle all rejects and errors
process.on('unhandledRejection', console.error);
