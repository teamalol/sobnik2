const easyVK = require("easyvk");
const config = require("./config");
let easyVKclient = null

const getVKClient = ()=>{
    return easyVKclient
}

const connect = async ()=>{
    easyVKclient = null
    easyVKclient = await easyVK(config.server.easyvk)
}

module.exports = {
    connect, getVKClient, easyVKclient
}
