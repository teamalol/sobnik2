'use strict'

const EventEmitter = require('events')
const emitter = new EventEmitter()
emitter.setMaxListeners(100)
const config = require('./config')
const easyVK = require('easyvk')
let cron = require('node-cron')
let count = 0
let DB = require('./DB')
const cities = require('./config/city').cities
const readline = require('readline')

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})

const captchaHandler = ({ captcha_sid, captcha_img, resolve: solve, vk }) => {
  rl.question(`Введите капчу для картинки ${captcha_img} `, key => {
    // Когда вводится капча в консоль, пробуем решить капчу
    solve(key)
      .then(() => {
        console.log('Капча решена корректно!')
      })
      .catch(({ err, reCall: tryNewCall }) => {
        // Иначе капча не решена, стоит попробовать снова
        console.log('Капче не решена!!!\nПробуем занова')

        tryNewCall() // Пробуем снова, занова запускаем наш captchaHandler, по факту...

        // Не стоит самостоятельно перезапускать функцию captchaHandler, так как в EasyVK
        // для этого имеется функция reCall, которая точно запустит все как нужно
      })
  })
}

function startApp() {
  console.log('Старт программы POSTER')

  count++

  function delay() {
    return new Promise(resolve => setTimeout(resolve, 10000))
  }

  function getHashTags(cityId) {
    return cities
      .filter(city => city.id === cityId)[0]
      .tags.map(city => '#' + city + ' ')
      .join(' ')
  }

  function getAttachments(post) {
    return !!post.attachments
      ? post.attachments
        .filter(attachment => attachment.type === 'photo')
        .map(attachment => `photo${attachment.photo.owner_id}_${attachment.photo.id}`)
        .join()
      : ''
  }

  async function postAd(post, vk) {
    await delay()
    vk.call('wall.post', {
      owner_id: cities.filter(city => city.id === post.city)[0].publicId * -1,
      message: `${post.text} \n ${getHashTags(
        post.city
      )} #Собственник #БезПосредников #сдам #сниму #БезКомиссии #sobnik sobnik.ru \n [id${post.user_id}|${post.first_name
        }] `,
      from_group: 1,
      attachments: getAttachments(post)
    })
      .then(resp => {
        console.log(resp)
      })
      .catch(err => {
        console.log(err)
      })
  }

  async function repostAd(post, vk) {
    //await delay()
    // vk.call('wall.post', {
    //   owner_id: cities.filter((city) => city.id === post.city)[0].publicId * -1,
    //   message: `${post.text} \n ${getHashTags(
    //     post.city
    //   )} #Собственник #БезПосредников #сдам #сниму #БезКомиссии #sobnik sobnik.ru \n [id${post.user_id}|${
    //     post.first_name
    //   }] `,
    //   from_group: 1,
    //   attachments: getAttachments(post),
    // })

    vk.call('wall.repost', {
      object: `wall${post.user_id}_${post.post_id}`,
      message: `${getHashTags(post.city)} #Собственник #БезПосредников #сдам #сниму sobnik.ru `,
      group_id: cities.filter(city => city.id === post.city)[0].publicId
    })
      .then(resp => {
        console.log(resp)
      })
      .catch(err => {
        console.log(err)
      })
  }

  config.server.easyvk.captchaHandler = captchaHandler
  easyVK(config.server.easyvk)
    .then(async vk => {
      let posts = await DB.getAds(1)
      let uniсPosts = posts.filter((thing, index, self) => self.findIndex(p => p.user_id === thing.user_id) === index)
      let postsFromUserWall = uniсPosts.filter(p => p.public_id == 0)
      let postsFromPublic = uniсPosts.filter(p => p.public_id > 0)

      for (const post of postsFromUserWall) {
        if (
          cities
            .filter(city => (!!city.publicId ? true : false))
            .map(city => city.id)
            .includes(post.city)
        ) {
          repostAd(post, vk)
        }
      }

      for (const post of postsFromPublic) {
        if (
          cities
            .filter(city => (!!city.publicId ? true : false))
            .map(city => city.id)
            .includes(post.city)
        ) {
          postAd(post, vk)
        }
      }
    })
    .then(res => {
      console.log(res)
    })

    .catch(console.error)
  //Handle all rejects and errors
  process.on('unhandledRejection', console.error)
}

let task = cron.schedule('*/30 * * * *', startApp)

task.start()

startApp()
