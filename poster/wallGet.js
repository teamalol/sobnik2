const easyVK = require('easyvk')
const config = require('./config')

let scriptArr = [
    'var posts = API.wall.get({"owner_id": 1, "count": 1});',
    'return posts;'
]
let scriptStr = scriptArr.join(' ')

easyVK(config.server.easyvk)
    .then(vk => {
        vk.call('execute', {
            code: scriptStr
        })
    })
    .then(respon => console.log('EXECUTE RESP:  ' + respon))