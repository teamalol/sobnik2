//config
const server = require('./server')
const { words, userWallsBlackWords } = require('./blackListWords')
const statusBlackList = require('./statuBlackList')

module.exports = {
  server: server,
  words: words,
  userWallsBlackWords,
  statusBlackList: statusBlackList,
}
