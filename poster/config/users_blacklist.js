let DB = require('./../DB')

let users_blacklist = async () => await DB.getUserBlacklist()

let usersBlacklist = {
  agents: [],
  usersPostManyAds: [],
  loadAgents() {
    // console.log(DB.getUserBlacklist());
    // this.agents = async () => await DB.getUserBlacklist();
    DB.pool
      .getConnection()
      .then(conn => {
        const query_str = 'SELECT * FROM ARENDA_DB.users_blacklist;'
        let ids = conn.query(query_str)
        conn.release()
        return ids
      })
      .then(res => res[0].map(el => el.vk_id))
      .then(res => {
        this.agents = res
      })
      .catch(err => {
        console.log(err)
      })
  },
  //TODO: пересмотреть потребность в данном методе
  loadUsersPostManyAds() {
    // console.log(DB.getUserBlacklist());
    // this.agents = async () => await DB.getUserBlacklist();
    DB.pool
      .getConnection()
      .then(conn => {
        const query_str = 'SELECT * FROM ARENDA_DB.users_post_many_ads;'
        let ids = conn.query(query_str)
        conn.release()
        return ids
      })
      .then(res => res[0].map(el => el.vk_id))
      .then(res => {
        this.usersPostManyAds = res
      })
      .catch(err => {
        console.log(err)
      })
  },
}

var users_post_many_ads = ['88888888888', '202152929']

module.exports = {
  users_blacklist: users_blacklist,
  users_post_many_ads: users_post_many_ads,
  usersBlacklist: usersBlacklist,
}
