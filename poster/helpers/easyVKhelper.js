const config = require('../config')
const easyVK = require('easyvk')

async function getUsersFromVk({ posts, signerIds }) {
  return easyVK(config.server.easyvk)
    .then(vk => {
      return vk.call('users.get', {
        user_ids: signerIds,
        fields: 'city, home_town, career, photo_50, status',
      })
    })
    .then(resp => {
      posts.profiles = posts.profiles.concat(resp.vkr.response)
      return posts
    })
}

async function getPublicName(id) {
  return easyVK(config.server.easyvk)
    .then(vk => {
      return vk.call('groups.getById', {
        group_ids: id,
      })
    })
    .then(resp => {
      return resp.vkr.response
    })
}

async function getGroupsByIds(workGroupIds) {
  return easyVK(config.server.easyvk)
    .then(vk => {
      return vk.call('groups.getById', {
        group_ids: workGroupIds,
        fields: 'description,city',
      })
    })
    .then(resp => {
      return resp.vkr.response
    })

  // for (let blackWord of config.careers.companyNames) {
  //     if (public[0].name.toLowerCase().search(blackWord) !== -1) {
  //         isAgent = true
  //         console.log('агент найден по названию рабочего паблика')
  //         agents.push(profile.id)
  //     }
  // }
}

module.exports = {
  getUsersFromVk,
  getGroupsByIds,
}
