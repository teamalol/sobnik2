const config = require('../config')
let pool = require('mysql2/promise').createPool(config.server.mysql)

function delay() {
  return new Promise((resolve) => setTimeout(resolve, 3000))
}

async function insertAdsIntoDb(posts) {
  for (let post of posts) {
    await delay()
    let images = []
    if (!!post.post.attachments) {
      if (post.post.attachments.length > 0) {
        for (const attach of post.post.attachments) {
          attach.type == 'photo' ? images.push(attach.photo.sizes[attach.photo.sizes.length - 1].url) : null
        }
      }
    }

    let queryString =
      'INSERT INTO ARENDA_DB.ads (text, city, user_id, post_id, avatar_url, public_id, ad_type, date, first_name, images) VALUES ("' +
      post.post.newtext +
      '",' +
      post.gorod.id +
      ',' +
      post.post.owner_id +
      ',' +
      post.post.id +
      ', "' +
      post.post.avatar +
      '",' +
      post.post.publicId +
      ',' +
      post.post.ad_type +
      ',' +
      post.post.date +
      ',"' +
      post.post.first_name +
      '",' +
      "'" +
      JSON.stringify(images) +
      "');"

    pool
      .query(queryString)
      .then(([result, fields]) => {
        console.log(result)
      })
      .catch((err) => {
        console.error(err)
      })
  }
}

function addUserBlacklist(users) {
  pool
    .getConnection()
    .then((conn) => {
      for (let user of users) {
        let query_str =
          'INSERT INTO ARENDA_DB.users_blacklist (vk_id, reason_id, date) VALUES (' +
          user.id +
          ',' +
          user.status +
          ',' +
          parseInt(Date.now() / 1000) +
          ');'
        console.log(query_str)
        conn.query(query_str)
      }
      conn.release()
    })
    .catch(console.error)
}

async function getUserBlacklist(res) {
  return pool
    .getConnection()
    .then((conn) => {
      const query_str = 'SELECT * FROM ARENDA_DB.users_blacklist;' //and reason_id>90 and reason_id<190
      let ids = conn.query(query_str)
      conn.release()
      return ids
    })
    .then((res) => res[0].map((el) => el.vk_id))
    .catch((err) => {
      console.log(err)
    })
}

async function getAds(type) {
  return pool
    .getConnection()
    .then(async (conn) => {
      const query_str = `SELECT * FROM ARENDA_DB.ads where ad_type=${type} and date>${
        parseInt(Date.now() / 1000) - 60 * 31
      };` //and reason_id>90 and reason_id<190

      let ads = await conn.query(query_str)
      conn.release()
      return ads[0]
    })
    .catch((err) => {
      console.log(err)
    })
}

module.exports = {
  insertAdsIntoDb: insertAdsIntoDb,
  addUserBlacklist: addUserBlacklist,
  pool: pool,
  getUserBlacklist,
  getAds,
}
