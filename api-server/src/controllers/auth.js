const jwt = require('jsonwebtoken')
const authHelper = require('../helpers/authHelper')
const { pool } = require('../config/DB')
const { config } = require('../config')

async function updateTokens(userId, role, firstName) {
  const accessToken = authHelper.generateAccessToken(userId, role, firstName),
    refreshToken = authHelper.generateRefreshToken()

  return await authHelper
    .replaceDbRefreshToken(refreshToken.id, userId)
    .then(() => ({
      accessToken,
      refreshToken: refreshToken.token
    }))
    .catch(err => {
      console.log(err)
    })
}

const signIn = (req, res, userId, photo_50, role, firstName) => {
  updateTokens(userId, role, firstName).then(tokens => {
    res.cookie('userPhoto', `${photo_50}`)
    res.cookie('accessToken', `${tokens.accessToken}`)
    res.cookie('refreshToken', `${tokens.refreshToken}`)
    res.cookie('firstName', `${firstName}`)
    role === 'admin' ? res.cookie('token', `${role}`) : null
    res.redirect(
      // `${config.app_sobnik.hostSobnik}/tokens/${tokens.accessToken}--${tokens.refreshToken}--${photo_50.substring(7)}${
      `${config.app_sobnik.hostSobnik}/msk/sdam`
    )
    // axios.post(`${config.app_sobnik.hostSobnik}/tokens/44`, tokens)
  })
}

const refreshTokens = (req, res) => {
  delete req.query.code // Проверить необходимость
  const refreshToken = req.headers.refreshtoken,
    payload = jwt.decode(refreshToken),
    queryString = `SELECT * FROM ARENDA_DB.tokens where token_id="${payload.id}";`

  pool
    .query(queryString)
    .then(([result, fields]) => {
      let token = result
      if (token.length > 0) {
        if (token[0].token_id === payload.id) {
          res.redirect(`${config.app_sobnik.hostSobnik}/main?tokens=`)
          // updateTokens(token[0].user_id)
          //   .then(tokens =>
          //     res.redirect(
          //       `${config.app_sobnik.hostSobnik}/main?tokens=` + tokens.accessToken + '--' + tokens.refreshToken
          //     )
          //   )
          //   .catch(err => {
          //     console.log(err)
          //   })
        } else res.sendStatus(401)
      } else {
        console.log('Refresh Token not found: пройдите авторизацию')
        res.sendStatus(401)
      }
    })
    // .then(token => {
    //   if (token[0] && token[0][0]) {
    //     if (token[0][0].token_id === payload.id) {
    //       updateTokens(token[0][0].user_id)
    //     } else res.redirect(`${config.app_sobnik.hosts}/auth/`)
    //   } else {
    //     console.log('Refresh Token not found: пройдите авторизацию')
    //     res.redirect(`${config.app_sobnik.hosts}/auth/`)
    //   }
    // })
    .catch(err => {
      console.log('Ошибка получения токена!')
      console.log(err)
      res.redirect(`${config.app_sobnik.hosts}/auth/`)
      // res.sendStatus(401)
    })
}

module.exports = {
  signIn,
  updateTokens,
  refreshTokens
}
