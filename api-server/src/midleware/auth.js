const jwt = require('jsonwebtoken'),
  config = require('../config').config

module.exports = (req, res, next) => {
  const authHeader = req.get('Authorization')
  if (!authHeader) {
    res.status(401).end()
    return
  }
  const token = authHeader.replace('Bearer ', '')
  const refreshToken = req.get('refreshToken')

  try {
    const payload = jwt.verify(token, config.app_sobnik.jwtSecret)
    if (payload.type !== 'access') {
      console.log('Invalid token')
      res.status(401).json({ message: 'Invalid token' })
    }
    req.bodyMidleWare = {}
    req.bodyMidleWare.userId = payload.userId
    req.bodyMidleWare.role = payload.role
    req.bodyMidleWare.firstName = payload.firstName

    // Сделать запрос в базу и разграничить доступ
    next()
  } catch (e) {
    if (e instanceof jwt.TokenExpiredError) {
      console.log('TokenExpired')
      res.status(401).json({ message: 'TokenExpired' })
      //res.redirect(`${config.app_sobnik.hosts}/auth/`)
      // axios
      //   .get(`${config.app_sobnik.hosts}/refresh-tokens`, {
      //     headers: { refreshToken: `${refreshToken}` },
      //   })
      //   .then(tokens => {
      //     try {
      //       jwt.verify(tokens.accessToken, config.app_sobnik.jwtSecret)
      //       next()
      //     } catch (e) {
      //       //res.redirect(`${config.app_sobnik.hosts}/auth/`)
      //       console.log('Invalid auth')
      //       res.status(401).json({ message: 'Invalid auth' })
      //       return
      //     }
      //   })
      //   .catch(err => {
      //     console.log(err)
      //   })
    } else if (e instanceof jwt.JsonWebTokenError) {
      console.log('JsonWebTokenError')
      console.log(e)
      res.status(401).json({ message: 'Invalid token!' })
      // res.redirect(`${config.app_sobnik.hosts}/auth/`)
      return
    }
  }
}
