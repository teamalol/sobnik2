const jwt = require('jsonwebtoken')
const config = require('../config').config

function deletePost(req, res, next) {
  const authHeader = req.get('Authorization')
  if (!authHeader) {
    res.status(401).end()
    return
  }
  const token = authHeader.replace('Bearer ', '')
  const payload = jwt.verify(token, config.app_sobnik.jwtSecret)

  if (req.body.user_id === payload.userId || payload.role === 'admin') {
    next()
  } else {
    res.status(403).end()
  }
}

module.exports = deletePost
