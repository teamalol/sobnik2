const vk = require('../config').config.vk

const getVkCode = (req, res) => {
  let queryString =
    'https://oauth.vk.com/authorize?' +
    'client_id=' +
    vk.client_id +
    '&redirect_uri=' +
    vk.redirect_uri +
    '&response_type=code&scope=photos&display=popup&v=5.103&state=777'
  // res.redirect(queryString);
  res.send({ queryString: queryString })
}

const getVkAccessTokenQuery = code => {
  let queryString =
    'https://oauth.vk.com/access_token?' +
    'client_id=' +
    vk.client_id +
    '&client_secret=' +
    vk.client_secret +
    '&redirect_uri=' +
    vk.redirect_uri +
    '&code=' +
    code +
    '&v=5.103'
  return queryString
}

module.exports = {
  getVkCode,
  getVkAccessTokenQuery,
}
