const jwt = require('jsonwebtoken'),
  uuid = require('uuid/v4'),
  { secret, tokens } = require('../config').config.app_sobnik.jwt,
  { pool } = require('../config/DB')

generateAccessToken = (userId, role, firstName) => {
  const payload = {
    userId,
    type: tokens.access.type,
    role,
    firstName,
  }
  const options = { expiresIn: tokens.access.expiresIn }

  return jwt.sign(payload, secret, options)
}

const generateRefreshToken = () => {
  const payload = {
    id: uuid(),
    type: tokens.refresh.type,
  }
  const options = { expiresIn: tokens.refresh.expiresIn }

  return {
    id: payload.id,
    token: jwt.sign(payload, secret, options),
  }
}

const replaceDbRefreshToken = (tokenId, userId) => {
  const queryReplace = `REPLACE INTO ARENDA_DB.tokens VALUES ("${tokenId}", ${userId}); `
  return pool
    .query(queryReplace)
    .then(result => {
      return result
    })
    .catch(err => {
      console.error('replaceDbRefreshToken: ', err)
    })
}

module.exports = {
  generateAccessToken,
  generateRefreshToken,
  replaceDbRefreshToken,
}
