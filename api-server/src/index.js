'use strict'
//@ts-check
let express = require('express')
const routes = require('./citys').routes
const { config } = require('./config/index')
process.env.NODE_ENV === 'production' ? '' : (process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0)
// process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0

let bodyParser = require('body-parser')
let app = express()
const axios = require('axios')
const authMiddleware = require('./midleware/auth')
let cors = require('cors')
const { getVkAccessTokenQuery, getVkCode } = require('./helpers/vkHelper')
const { pool } = require('./config/DB')
const auth = require('./controllers/auth')
let multer = require('multer')
const acl = require('./acl')
const easyVK = require('easyvk')

// app.use(function (req, res, next) {
//   var origins = [
//     'https://sobnik.com',
//     'https://sobnik.ru',
//     'https://sobnik.ru/auth/',
//     'http://127.0.0.1:3000',
//     'http://localhost:3000',
//     '176.53.160.132:443'
//   ]

//   for (var i = 0; i < origins.length; i++) {
//     var origin = origins[i]

//     if (req.headers.origin.indexOf(origin) > -1) {
//       res.header('Access-Control-Allow-Origin', req.headers.origin)
//     }
//   }

//   res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE')
//   res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
//   next()
// })

app.use(bodyParser.json())

app.use(
  bodyParser.urlencoded({
    extended: true
  })
)

axios.defaults.headers.common.Accept = 'application/json'
axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*'

let whitelist = [
  'https://sobnik.ru/auth/',
  'https://sobnik.com',
  'http://127.0.0.1:3000',
  'http://localhost:3000',
  '176.53.160.132:443',
  'https://sobnik.ru',
  'http://sobnik.ru'
]

let corsOptions = {
  methods: ['POST', 'GET', 'DELETE', 'PUT', 'OPTIONS'],
  credentials: true,
  origin: true

  // function (origin, callback) {
  //   // if (whitelist.indexOf(origin) !== -1) {
  //   callback(null, true)
  //   // } else {
  //   //   callback(new Error('Not allowed by CORS'))
  //   // }
  // }
}

app.use(cors(corsOptions))
app.get('/api/auth/', function (req, res) {
  getVkCode(req, res)
})

app.get('/api/auth2/', function (req, res) {
  axios
    .get(getVkAccessTokenQuery(req.query.code))
    .then(response => {
      delete req.query.code // Проверить необходимость
      let userId = response.data.user_id
      if (userId > 0) {
        const queryString = `SELECT * FROM ARENDA_DB.users_blacklist where vk_id=${userId};` //and reason_id>90 and reason_id<190
        pool
          .query(queryString)
          .then(([agents, fields]) => {
            if (agents.includes(userId || userId > 530000000)) {
              console.log('Доступ запрещен')
              res.redirect(`${config.app_sobnik.hostSobnik}/access_denied`)
            } else {
              easyVK({
                access_token: response.data.access_token
              })
                .then(vk => {

                  return vk
                    .call('users.get', {
                      user_ids: userId.toString(),
                      fields: 'photo_50'
                    })
                    .then(userVk => {
                      let queryString =
                        'SELECT * FROM ARENDA_DB.users u, ARENDA_DB.role r WHERE u.vk_id=' +
                        userId +
                        ' AND u.role_id = r.role_id;'

                      pool
                        .query(queryString)
                        .then(([result, fields]) => {
                          if (result) {
                            let role = !!result[0] ? result[0].role_name : null
                            auth.signIn(req, res, userId, userVk.photo_50, role, userVk.first_name)
                          }
                        })
                        .catch(err => {
                          console.error(err)
                          res.sendStatus(500)
                        })
                    })
                })
                .catch(console.error)
            }
          })
          .catch(err => {
            // проверить data
            console.error(err)
            res.redirect(`${config.app_sobnik.hostSobnik}/auth_error`)
          })
      }
    })
    .catch(err => {
      // TODO: сделать нормальную обработку ошибок
      console.log(req.query)
      console.error(err)
      if (!!err.response.status && err.response.status == 401) {
        res.redirect(`${config.app_sobnik.hostSobnik}/auth_error`)
      } else res.sendStatus(500)
    })
})

app.post('/api/ads/list/:limit', authMiddleware, function (req, res) {
  let adType = req.body.adType === 0 ? '(ad_type=0 or ad_type=10)' : '(ad_type=1 or ad_type=11)',
    queryString = `SELECT id, user_id, text, avatar_url, public_id, post_id, ad_type, date, first_name, images FROM ARENDA_DB.ads where city=${req.body.citySelected} AND ${adType} ORDER BY id DESC LIMIT 
${req.params.limit};`
  pool
    .query(queryString)
    .then(([ads, fields]) => {
      res.send(ads)
    })
    .catch(err => {
      res.sendStatus(500)
      console.error(err)
    })
})

app.get('/api/ads/getPostById/:id', authMiddleware, function (req, res) {
  let queryString = `SELECT * FROM ARENDA_DB.ads where id=${req.params.id};`
  pool
    .query(queryString)
    .then(([ads, fields]) => {
      res.send(ads)
    })
    .catch(err => {
      res.sendStatus(500)
      console.error(err)
    })
})

app.post('/api/ads/delete', authMiddleware, acl.deletePost, function (req, res) {
  let queryString = 'DELETE FROM ARENDA_DB.ads where id=' + req.body.id + ' LIMIT 1;'
  pool
    .query(queryString)
    .then(([result, fields]) => {
      res.send(result)
    })
    .catch(err => {
      console.error(err)
      res.sendStatus(500)
    })
})

app.post('/api/ads/deleteAllAdsOfUser', authMiddleware, acl.deletePost, function (req, res) {
  let queryString = 'DELETE FROM ARENDA_DB.ads where user_id=' + req.body.userId + ';'
  pool
    .query(queryString)
    .then(([result, fields]) => {
      res.send(result)
    })
    .catch(err => {
      console.error(err)
      res.sendStatus(500)
    })
})

app.post('/api/user/block', authMiddleware, acl.deletePost, function (req, res) {
  let queryString =
    'INSERT INTO ARENDA_DB.users_blacklist (vk_id,reason_id, date) VALUES (' +
    req.body.userId +
    ',' +
    req.body.reasonId +
    ',' +
    parseInt(Date.now() / 1000) +
    ')'
  pool
    .query(queryString)
    .then(([result, fields]) => {
      res.send(result)
    })
    .catch(err => {
      console.error(err)
      res.sendStatus(500)
    })
})

app.post('/api/vk/callback', function (req, res) {
  switch (req.group_id) {
    case 19731546:
      res.send('ef66bf55')
      break
    case 68780309:
      res.send('770e501a')
      break

    default:
      res.send('empty')
      break
  }
})

app.get('/api/ads/admin', authMiddleware, acl.deletePost, function (req, res) {
  let queryString = `SELECT id, text, avatar_url, public_id, post_id, ad_type, user_id, city, date, first_name, images FROM ARENDA_DB.ads where date>${req.body.dateFrom} and date<${req.body.dateTo} ORDER BY id DESC;`
  pool
    .query(queryString)
    .then(([ads, fields]) => {
      res.send(ads)
    })
    .catch(err => {
      console.error(err)
      res.sendStatus(500)
    })
})

app.get('/api/complains', authMiddleware, acl.deletePost, function (req, res) {
  let queryString = `SELECT * FROM ARENDA_DB.complains ORDER BY id DESC;`

  pool
    .query(queryString)
    .then(([result, fields]) => {
      res.send(result)
    })
    .catch(err => {
      console.error(err)
      res.sendStatus(500)
    })
})


const { SitemapStream, streamToPromise } = require('sitemap')
const { createGzip } = require('zlib')

let sitemap

app.get('/sitemap.xml', function (req, res) {
  res.header('Content-Type', 'application/xml')
  res.header('Content-Encoding', 'gzip')
  // if we have a cached entry send it
  if (sitemap) {
    res.send(sitemap)
    return
  }

  let ad_types = ['snimu', 'sdam']
  let queryString = 'SELECT id, ad_type, city FROM ARENDA_DB.ads order by id desc limit 10000;'

  pool
    .query(queryString)
    .then(([result, fields]) => {
      let routeIds = routes.map(r => r.id)
      let posts = result.filter(p => routeIds.includes(p.city))

      let postArray = posts
        .sort((a, b) => {
          return a.city - b.city
        })
        .map(p => `/${routes.filter(r => r.id === p.city)[0].name}/${ad_types[p.ad_type]}/${p.id}`)
      try {
        const smStream = new SitemapStream({ hostname: 'https://sobnik.ru/' })
        const pipeline = smStream.pipe(createGzip())


        postArray = [
          '/',
          '/contacts',
          ...routes.map(route => `/${route.name}/sdam`),
          ...routes.map(route => `/${route.name}/snimu`),
          ...postArray
        ]
        postArray.forEach(item => smStream.write(item))
        // cache the response
        streamToPromise(pipeline).then(sm => (sitemap = sm))
        // make sure to attach a write stream such as streamToPromise before ending
        smStream.end()
        // stream write the response
        pipeline.pipe(res).on('error', e => {
          throw e
        })
      } catch (e) {
        console.error(e)
        res.status(500).end()
      }
      // res.send(result)
    })
    .catch(err => {
      console.error(err)
      res.sendStatus(500)
    })
})

app.post('/api/ads/complain', authMiddleware, function (req, res) {
  let queryString = `INSERT INTO ARENDA_DB.complains (user_id, user_from_id, complain_type_id, ads_id, date) VALUES (${req.body.user_id
    }, ${req.bodyMidleWare.userId}, ${req.body.complainTypeId}, ${req.body.id}, ${Date.now() / 1000})`

  pool
    .query(queryString)
    .then(([result, fields]) => {
      res.send(result)
    })
    .catch(err => {
      console.error(err)
      res.sendStatus(500)
    })
})

app.get('/api/ads/account', authMiddleware, function (req, res) {
  let queryString = `SELECT id, text, avatar_url, public_id, post_id, ad_type, user_id, city, date, first_name, images FROM ARENDA_DB.ads where user_id=${req.bodyMidleWare.userId} ORDER BY id DESC;`
  pool
    .query(queryString)
    .then(([ads, fields]) => {
      res.send(ads)
    })
    .catch(err => {
      console.error(err)
      res.sendStatus(500)
    })
})

app.post('/api/ads/limit/', function (req, res) {
  let adType = req.body.adType === 0 ? '(ad_type=0 or ad_type=10)' : '(ad_type=1 or ad_type=11)',
    queryString = `SELECT id, user_id, text, avatar_url, public_id, post_id, ad_type, date, first_name, images FROM ARENDA_DB.ads where city=${req.body.citySelected} AND (ad_type=1 or ad_type=11) ORDER BY id DESC LIMIT 5;`
  pool
    .query(queryString)
    .then(([ads, fields]) => {
      res.send(ads)
    })
    .catch(err => {
      res.sendStatus(500)
      console.error(err)
    })
})

app.post('/api/payment/result', function (req, res) {
  console.log('payment/result')
  console.log(req)
  res.sendStatus(200)
})

app.post('/api/payment/success', function (req, res) {
  console.log('payment/success')
  console.log(req)
  res.sendStatus(200)
})

app.post('/api/payment/error', function (req, res) {
  console.log('payment/error')
  console.log(req)
  res.sendStatus(200)
})

app.post('/api/add_post/', authMiddleware, (req, res) => {
  function sanitizeFile(file, cb) {
    let fileExts = ['png', 'jpg', 'jpeg', 'gif']
    let isAllowedExt = fileExts.includes(file.originalname.split('.')[1].toLowerCase())
    // Mime type must be an image
    let isAllowedMimeType = file.mimetype.startsWith('image/')
    if (isAllowedExt && isAllowedMimeType) {
      return cb(null, true) // no errors
    } else {
      // pass error msg to callback, which can be displaye in frontend
      cb('Error: File type not allowed!')
    }
  }

  const storage = multer.diskStorage({
    destination: config.app_sobnik.directoryStatic,
    filename: function (req, file, cb) {
      // null as first argument means no error
      cb(null, Date.now() + '.' + file.originalname.split('.')[1])
    }
  })

  const upload = multer({
    storage: storage,
    limits: {
      fileSize: 100000000
    },
    fileFilter: function (req, file, cb) {
      sanitizeFile(file, cb)
    }
  }).array('images')

  upload(req, res, err => {
    if (err) {
      console.log(err)
    } else {
      // If file is not selected

      let images = []
      for (const image of req.files) {
        images.push(config.app_sobnik.hostStatic + image.filename)
      }

      let queryString = `INSERT INTO ARENDA_DB.ads (text, city, user_id, images, avatar_url, ad_type, post_id, date, first_name) VALUES ("
          ${req.body.postText}", ${req.body.postCity}, ${req.bodyMidleWare.userId}, '${JSON.stringify(images)}','${req.body.userPhoto
        }',${req.body.postType}, 0, ${parseInt(Date.now() / 1000)}, "${req.bodyMidleWare.firstName}");`

      pool
        .query(queryString)
        .then(() => {
          let querySelect = `SELECT * FROM ARENDA_DB.ads where user_id=${req.bodyMidleWare.userId}`
          pool
            .query(querySelect)
            .then(([ads, fields]) => {
              res.send(ads)
            })
            .catch(err => {
              console.error(err)
              res.sendStatus(404)
            })
        })

        .catch(err => {
          console.error(err)
          res.sendStatus(404)
        })
    }
  })
})

app.get('/api/ads/:id', function (req, res) {
  let queryString = `SELECT text,id,avatar_url,images FROM ARENDA_DB.ads where id=${req.params.id};`
  pool
    .query(queryString)
    .then(([ads, fields]) => {
      res.send(ads)
    })
    .catch(err => {
      res.sendStatus(500)
      console.error(err)
    })
})

app.post('/api/edit_post/', authMiddleware, (req, res) => {
  function sanitizeFile(file, cb) {
    let fileExts = ['png', 'jpg', 'jpeg', 'gif']
    let isAllowedExt = fileExts.includes(file.originalname.split('.')[1].toLowerCase())
    // Mime type must be an image
    let isAllowedMimeType = file.mimetype.startsWith('image/')
    if (isAllowedExt && isAllowedMimeType) {
      return cb(null, true) // no errors
    } else {
      // pass error msg to callback, which can be displaye in frontend
      cb('Error: File type not allowed!')
    }
  }

  const storage = multer.diskStorage({
    destination: config.app_sobnik.directoryStatic,
    filename: function (req, file, cb) {
      // null as first argument means no error
      cb(null, Date.now() + '.' + file.originalname.split('.')[1])
    }
  })

  const upload = multer({
    storage: storage,
    limits: {
      fileSize: 100000000
    },
    fileFilter: function (req, file, cb) {
      sanitizeFile(file, cb)
    }
  }).array('images')

  upload(req, res, err => {
    if (err) {
      console.log(err)
    } else {
      // If file is not selected

      let images = []
      for (const image of req.files) {
        images.push(config.app_sobnik.hostStatic + image.filename)
      }

      let imageString = images.length > 0 ? `images='${JSON.stringify(images)}',` : ''

      let queryString = `UPDATE ARENDA_DB.ads SET text='${req.body.postText}', city=${req.body.postCity}, user_id=${req.bodyMidleWare.userId
        }, ${imageString} avatar_url='${req.body.userPhoto}', ad_type=${req.body.postType}, date=${parseInt(
          Date.now() / 1000
        )}, first_name='${req.bodyMidleWare.firstName}' where id=${req.body.id} `

      pool
        .query(queryString)
        .then(() => {
          let querySelect = `SELECT * FROM ARENDA_DB.ads where user_id=${req.bodyMidleWare.userId}`
          pool
            .query(querySelect)
            .then(([ads, fields]) => {
              res.send(ads)
            })
            .catch(err => {
              console.error(err)
              res.sendStatus(404)
            })
        })

        .catch(err => {
          console.error(err)
          res.sendStatus(404)
        })
    }
  })
})

app.listen(5000, '0.0.0.0')
