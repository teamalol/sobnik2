const easyVK = require("easyvk");
const config = require("./config");
let easyVKclient =  null

const getVKClient = ()=>{
    return easyVKclient
}

const connect = async ()=>{
    try {
        easyVKclient = await easyVK(config.server.easyvk)
    } catch (error) {
        console.log(error)
    }

}

module.exports = {
    connect, getVKClient, easyVKclient
}
