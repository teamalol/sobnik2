const config = require('../config')
let pool = require('mysql2/promise').createPool(config.server.mysql)


function addUserBlacklist(users) {
  pool
    .getConnection()
    .then((conn) => {
      for (let user of users) {
        let query_str =
          'INSERT INTO default_db.users_blacklist (vk_id, reason_id, date) VALUES (' +
          user.id +
          ',' +
          user.status +
          ',' +
          parseInt(Date.now() / 1000) +
          ');'
        console.log(query_str)
        conn.query(query_str)
      }
      conn.release()
    })
    .catch(console.error)
}

async function getUserBlacklist(res) {
  return pool
    .getConnection()
    .then((conn) => {
      const query_str = 'SELECT * FROM default_db.users_blacklist;' //and reason_id>90 and reason_id<190
      let ids = conn.query(query_str)
      conn.release()
      return ids
    })
    .then((res) => res[0].map((el) => el.vk_id))
    .catch((err) => {
      console.log(err)
    })
}

async function getAds(cities) {
    const date = parseInt(Date.now() / 1000)
    console.log('DATE: ', date)
    return pool
    .getConnection()
    .then(async (conn) => {
      const query_str = `SELECT * FROM default_db.ads where (${cities
        .map((c) => 'city=' + c)
        .join(' or ')}) and date>${date - 60 * 61};` //and reason_id>90 and reason_id<190

      let ads = await conn.query(query_str)
      conn.release()
      return ads[0]
    })
    .catch((err) => {
      console.log(err)
    })
}

module.exports = {
  addUserBlacklist,
  pool: pool,
  getUserBlacklist,
  getAds,
}
