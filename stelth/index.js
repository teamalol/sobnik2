'use strict'

const EventEmitter = require('events')

const emitter = new EventEmitter()
emitter.setMaxListeners(0)
let cron = require('node-cron')
let count = 0
let DB = require('./DB')
const cities = require('./config/city').cities
const { getVKClient, connect } = require('./client')
const {citiesById} = require("./config/city");




const  cityIds =  cities
    .filter((city) => (!!city.publicId ? true : false))
    .map((city) => city.id)

const getHashTags = (cityId) => citiesById[cityId]?.tags.map((city) => `#${city}`)
        .join(' ')

function getAttachments(post) {
    return !!post.attachments
        ? post.attachments
            .filter((attachment) => attachment.type === 'photo')
            .map((attachment) => `photo${attachment.photo.owner_id}_${attachment.photo.id}`)
            .join()
        : ''
}


function startApp() {
    console.log('Старт программы STELTH')
    count++

    function delay() {
        return new Promise((resolve) => setTimeout(resolve, 10000))
    }

    async function postAdStelth(post) {
        let dateNow = parseInt(Date.now() / 1000)
        await delay()

        getVKClient().call('wall.post', {
            owner_id: cities.filter((city) => city.id === post.city)[0].publicId * -1,
            message: `${post.text} \n ${getHashTags(post.city)} \n \n [id${post.user_id}|${post.first_name}]`,
            attachments: getAttachments(post),
            publish_date: dateNow + 500000,
        })
            .then((resp) => {
                console.log(resp)
            })
            .catch((err) => {
                console.log(err)
            })
    }

    Promise.resolve()
        .then(async () => {
            let posts = await DB.getAds(cityIds)

            let uniсPosts = posts.filter((thing, index, self) => self.findIndex((p) => p.user_id === thing.user_id) === index)

            for (const post of uniсPosts) {
                    await postAdStelth(post)
            }
        })
        .then((res) => {
            console.log(res)
        })
        .catch(console.error)
    //Handle all rejects and errors
    process.on('unhandledRejection', console.error)
}


const start = async ()=> {
   await connect()

  cron.schedule("1 */1 * * *", startApp)
    startApp()
}

try {
    start()
} catch (error) {
    console.log(error)
}


//
// async function logInWith2Auth (params) {
//     return new Promise((_2faNeed) => {
//
//         function relogIn (_2faCode = "") {
//
//             if (_2faCode) params.code = _2faCode
//
//             easyVK(params).then(start).catch((err) => {
//                 console.log(err)
//
//                 if (!err.easyvk_error) {
//                     if (err.error_code == "need_validation") {
//                         _2faNeed({
//                             err: err,
//                             relogIn: relogIn
//                         });
//                     }
//
//                 }
//
//             })
//         }
//         relogIn()
//
//     })
// }
//
// logInWith2Auth({...config.server.easyvk, captchaHandler})
//     .then(({err: error, relogIn}) => {
//
//         console.log(error.validation_type);
//
//         rl.question(error.error + " ", (answer) => {
//
//             let code = answer;
//
//             relogIn(code);
//
//             rl.close();
//
//         });
//
//     })
